﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Prism.Commands;
using Prism.Mvvm;

namespace FilmwBud
{
    public class PlayerViewModel : BindableBase
    {
        private MainWindow View;
        private readonly PlayerModel _model = new PlayerModel();
        public PlayerViewModel(MainWindow View)
        {
            this.View = View;
            PauseCommand = new DelegateCommand(
                () => {
                    _model.Playing = !_model.Playing;
                    //_model.PauseVideo();
                }
            );
            StartCommand = new DelegateCommand(
                () => {
                    _model.Started = !_model.Started;
                    //_model.StartVideo();
                }
            );
            _model.onStart += ViewStart;
            _model.onUnpause += ViewUnpause;
            _model.onStop += ViewStop;
            _model.onPause += ViewPause;
        }
        public DelegateCommand PauseCommand { get; }
        public DelegateCommand StartCommand { get; }

        //TODO: functions which works both with view on events trigger
        private void ViewStart()
        {
            View.Player.Play();
            View.PauseButton.Visibility = Visibility.Visible;

            View.UnpauseButton.Visibility = Visibility.Collapsed;
            View.PlayButton.Visibility = Visibility.Collapsed;
        }

        private void ViewUnpause()
        {
            View.Player.Play();

            View.PauseButton.Visibility = Visibility.Visible;

            View.UnpauseButton.Visibility = Visibility.Collapsed;
            View.PlayButton.Visibility = Visibility.Collapsed;
        }

        private void ViewPause()
        {
            View.Player.Pause();

            View.UnpauseButton.Visibility = Visibility.Visible;

            View.PlayButton.Visibility = Visibility.Collapsed;
            View.PauseButton.Visibility = Visibility.Collapsed;
        }
        private void ViewStop()
        {
            View.Player.Stop();
            View.Player.Close();

            View.PlayButton.Visibility = Visibility.Visible;

            View.PauseButton.Visibility = Visibility.Collapsed;
            View.UnpauseButton.Visibility = Visibility.Collapsed;
        }

    }
}
