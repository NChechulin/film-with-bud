﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;

namespace FilmwBud
{
    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string VideoPath = "C:/video_with_bud/media.mp4";
        private DispatcherTimer _updateTimer;
        private bool _fullscreen = false;
        private bool _userUpdatingTimeline = false;
        private bool _isPlaying = false;

        public MainWindow()
        {
            InitializeComponent();

            PlayerViewModel vm = new PlayerViewModel(this);
            this.DataContext = vm;

            Init();
        }

        private void Init()
        {
            Player.Source = new Uri(VideoPath, UriKind.Absolute);


            _updateTimer = new DispatcherTimer();
            _updateTimer.Tick += UpdateTimeline_Tick;
            _updateTimer.Interval = new TimeSpan(0, 0, 0, 0, 50);
        }

        private void Pause()
        {
            Player.Pause();
            PlayButton.Visibility = Visibility.Visible;
            PauseButton.Visibility = Visibility.Collapsed;

            _isPlaying = false;
        }

        private void Unpause()
        {
            Player.Play();
            PlayButton.Visibility = Visibility.Collapsed;
            PauseButton.Visibility = Visibility.Visible;

            _isPlaying = true;
        }

        private void Stop()
        {
            Player.Stop();
            Player.Close();

            _isPlaying = false;
        }

        private void EnterFullscreen()
        {
            WindowStyle = WindowStyle.None;
            WindowState = WindowState.Maximized;
            MainGrid.RowDefinitions[1].Height = new GridLength(0.25, GridUnitType.Star); // this makes GUI dockpanel smaller
            ExitFullscreenButton.Visibility = Visibility.Visible;
            _fullscreen = true;
        }

        private void ExitFullscreen()
        {
            WindowStyle = WindowStyle.SingleBorderWindow;
            WindowState = WindowState.Normal;
            MainGrid.RowDefinitions[1].Height = new GridLength(1, GridUnitType.Star); // this returns gui size to minimize the window
            ExitFullscreenButton.Visibility = Visibility.Collapsed;
            _fullscreen = false;
        }



        private void ScrollForward10()
        {
            MovePlayerPosition(10000);
        }

        private void ScrollBackward10()
        {
            MovePlayerPosition(-10000);
        }


        private void MovePlayerPosition(int deltaChangeMilliseconds)
        {
            Pause();
            int targetPositionTotalMilliseconds = (int)Player.Position.TotalMilliseconds + deltaChangeMilliseconds;
            int totalMilliseconds = (int)Player.NaturalDuration.TimeSpan.TotalMilliseconds;

            targetPositionTotalMilliseconds = Math.Max(0, targetPositionTotalMilliseconds);
            targetPositionTotalMilliseconds = Math.Min(totalMilliseconds, targetPositionTotalMilliseconds);

            Player.Position += new TimeSpan(0, 0, 0, 0, deltaChangeMilliseconds);
            Timeline.Value = Player.Position.TotalMilliseconds;

            Unpause();
        }


        private void StopButton_Clicked(object sender, RoutedEventArgs e)
        {
            Stop();
        }

        private void PlayButton_Clicked(object sender, RoutedEventArgs e)
        {
            Unpause();
        }

        public void PauseVideo()
        {
            Pause();
        }

        private void Player_OnMediaOpened(object sender, RoutedEventArgs e)
        {
            if (Player.NaturalDuration.HasTimeSpan)
            {
                var totalMilliseconds = Player.NaturalDuration.TimeSpan.TotalMilliseconds;
                Timeline.Maximum = totalMilliseconds;
                _updateTimer.Start();
            }
            else
            {
                Debug.WriteLine("Player.NaturalDuration has no timespan");
                // this throws exception even if video is OK
                //throw new Exception("Player.NaturalDuration has no timespan");
            }
        }

        private void Player_OnMediaEnded(object sender, RoutedEventArgs e)
        {
            Stop();
        }

        private void UpdateTimeline_Tick(object sender, EventArgs e)
        {
            if (!_userUpdatingTimeline)
                Timeline.Value = Player.Position.TotalMilliseconds;
        }

        private void MainWindow_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
                EnterFullscreen();
            else if (_fullscreen)
                ExitFullscreen();
        }

        private void ExitFullscreenButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (!_fullscreen)
                EnterFullscreen();
            else
                ExitFullscreen();
        }


        private void KeyPressedOnForm(object sender, KeyEventArgs e)
        {
            //Debug.WriteLine($"PRESSED KEY {e.Key}");
            switch (e.Key)
            {
                case Key.F:
                    {
                        if (_fullscreen)
                            ExitFullscreen();
                        else
                            EnterFullscreen();
                        break;
                    }
                case Key.Space:
                    {
                        if (_isPlaying)
                            Pause();
                        else
                            Unpause();
                        break;
                    }
                case Key.M:
                    {
                        Player.IsMuted = !Player.IsMuted;
                        break;
                    }
                case Key.Right:
                    {
                        ScrollForward10();
                        break;
                    }
                case Key.Left:
                    {
                        ScrollBackward10();
                        break;
                    }

            }
        }

        private void Timeline_DragStarted(object sender, DragStartedEventArgs e)
        {
            _userUpdatingTimeline = true;
            Pause();
        }

        private void Timeline_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            double deltaChangeMilliseconds = Timeline.Maximum * e.HorizontalChange / Timeline.RenderSize.Width;
            MovePlayerPosition((int)deltaChangeMilliseconds);

            Unpause();
            _userUpdatingTimeline = false;
        }


        private void Player_OnMediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            MessageBox.Show("Error opening file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            throw new Exception("Failed opening mediafile");
        }
    }
}