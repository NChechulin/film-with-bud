﻿using Prism.Common;
using Prism.Mvvm;
using System;
using System.ComponentModel;
using System.IO;
using System.Net;

namespace FilmwBud
{
    public class PlayerModel
    {
        public delegate void PauseContainer();
        public delegate void UnpauseContainer();
        public delegate void StopContainer();
        public delegate void StartContainer();

        public event PauseContainer onPause;
        public event UnpauseContainer onUnpause;
        public event StopContainer onStop;
        public event StartContainer onStart;

        private TimeSpan _currentTime;
        private readonly TimeSpan _totalTime;

        public bool IsPlaying;
        public bool IsStarted;

        private string _filePath;

        void Init()
        {
            _currentTime = TimeSpan.Zero;
            IsPlaying = false;
            IsStarted = false;
        }

        public bool Playing
        {
            get => IsPlaying;

            set
            {
                if (IsStarted)
                {
                    IsPlaying = value;
                    // TODO: send callback for pause/unpause
                    if (IsPlaying)
                    {
                        onUnpause();
                    }
                    else
                    {
                        onPause();
                    }
                }
                else
                {
                    // TODO: raise exception
                }
            }
        }

        public bool Started
        {
            get => IsStarted;

            set
            {
                if (IsStarted != value)
                {
                    IsStarted = value;
                    // TODO: send callback
                    if (IsStarted)
                    {
                        IsPlaying = true;
                        onStart();
                    }
                    else
                    {
                        IsPlaying = false;
                        onStop();
                    }
                }
                else
                {
                    // TODO: raise exception
                }
            }
        }

        private string TimeSpanToString(TimeSpan time)
        {
            string FillWithZeros(int timeVal)
            {
                return timeVal.ToString().PadLeft(2, '0');
            }

            return $"{FillWithZeros(time.Hours)}:{FillWithZeros(time.Minutes)}:{FillWithZeros(time.Seconds)}";
        }

        public string GetCurrentTime => TimeSpanToString(_currentTime);

        public string GetTotalTime => TimeSpanToString(_totalTime);

        public string FilePath
        {
            get => _filePath;
            set
            {
                if (File.Exists(value))
                {
                    // TODO: Set this file to MediaElement
                    // TODO: Get file metadata (such as duration)
                    _filePath = value;
                }
                else
                {
                    // TODO: Raise no such file exception
                }
            }
        }
    }
}

